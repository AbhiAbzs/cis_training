class demoClass
{
    public static void fun()
    {
        System.Console.WriteLine("Hi from demo Fun.");
        System.Console.WriteLine(System.Console.Read());
    }
}
class namespaceConf
{
    static void Main()
    {
        System.Console.Clear();
        System.Console.WriteLine("From namespace conflict.");
        demoClass.fun();
    }
}