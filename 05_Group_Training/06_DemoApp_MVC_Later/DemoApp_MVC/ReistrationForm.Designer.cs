﻿namespace DemoApp
{
    partial class FormCrud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
    private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCrud));
            this.panel_crud_tabs = new System.Windows.Forms.Panel();
            this.tabCtrl_crud = new System.Windows.Forms.TabControl();
            this.tab_Reg = new System.Windows.Forms.TabPage();
            this.panel_Reg_Form = new System.Windows.Forms.Panel();
            this.lbl_Reg_Title = new System.Windows.Forms.Label();
            this.rich_txt_box_address = new System.Windows.Forms.RichTextBox();
            this.BtnRegister = new System.Windows.Forms.Button();
            this.msk_txt_box_pass = new System.Windows.Forms.MaskedTextBox();
            this.txt_box_lname = new System.Windows.Forms.TextBox();
            this.txt_box_fname = new System.Windows.Forms.TextBox();
            this.txt_box_usname = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_lname = new System.Windows.Forms.Label();
            this.lbl_fname = new System.Windows.Forms.Label();
            this.lbl_pass = new System.Windows.Forms.Label();
            this.lbl_uname = new System.Windows.Forms.Label();
            this.tab_Retrieve = new System.Windows.Forms.TabPage();
            this.tab_Update = new System.Windows.Forms.TabPage();
            this.Tab_Delete = new System.Windows.Forms.TabPage();
            this.panel_crud_tabs.SuspendLayout();
            this.tabCtrl_crud.SuspendLayout();
            this.tab_Reg.SuspendLayout();
            this.panel_Reg_Form.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_crud_tabs
            // 
            this.panel_crud_tabs.AutoSize = true;
            this.panel_crud_tabs.Controls.Add(this.tabCtrl_crud);
            this.panel_crud_tabs.Location = new System.Drawing.Point(3, 0);
            this.panel_crud_tabs.Margin = new System.Windows.Forms.Padding(4);
            this.panel_crud_tabs.Name = "panel_crud_tabs";
            this.panel_crud_tabs.Size = new System.Drawing.Size(939, 518);
            this.panel_crud_tabs.TabIndex = 13;
            // 
            // tabCtrl_crud
            // 
            this.tabCtrl_crud.Controls.Add(this.tab_Reg);
            this.tabCtrl_crud.Controls.Add(this.tab_Retrieve);
            this.tabCtrl_crud.Controls.Add(this.tab_Update);
            this.tabCtrl_crud.Controls.Add(this.Tab_Delete);
            this.tabCtrl_crud.Location = new System.Drawing.Point(4, 4);
            this.tabCtrl_crud.Margin = new System.Windows.Forms.Padding(4);
            this.tabCtrl_crud.Name = "tabCtrl_crud";
            this.tabCtrl_crud.Padding = new System.Drawing.Point(6, 4);
            this.tabCtrl_crud.SelectedIndex = 0;
            this.tabCtrl_crud.Size = new System.Drawing.Size(739, 448);
            this.tabCtrl_crud.TabIndex = 0;
            // 
            // tab_Reg
            // 
            this.tab_Reg.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Reg.Controls.Add(this.panel_Reg_Form);
            this.tab_Reg.Location = new System.Drawing.Point(4, 27);
            this.tab_Reg.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Reg.Name = "tab_Reg";
            this.tab_Reg.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Reg.Size = new System.Drawing.Size(731, 417);
            this.tab_Reg.TabIndex = 0;
            this.tab_Reg.Tag = "";
            this.tab_Reg.Text = "Register";
            // 
            // panel_Reg_Form
            // 
            this.panel_Reg_Form.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel_Reg_Form.Controls.Add(this.lbl_Reg_Title);
            this.panel_Reg_Form.Controls.Add(this.rich_txt_box_address);
            this.panel_Reg_Form.Controls.Add(this.BtnRegister);
            this.panel_Reg_Form.Controls.Add(this.msk_txt_box_pass);
            this.panel_Reg_Form.Controls.Add(this.txt_box_lname);
            this.panel_Reg_Form.Controls.Add(this.txt_box_fname);
            this.panel_Reg_Form.Controls.Add(this.txt_box_usname);
            this.panel_Reg_Form.Controls.Add(this.lbl_address);
            this.panel_Reg_Form.Controls.Add(this.lbl_lname);
            this.panel_Reg_Form.Controls.Add(this.lbl_fname);
            this.panel_Reg_Form.Controls.Add(this.lbl_pass);
            this.panel_Reg_Form.Controls.Add(this.lbl_uname);
            this.panel_Reg_Form.Location = new System.Drawing.Point(0, 0);
            this.panel_Reg_Form.Margin = new System.Windows.Forms.Padding(4);
            this.panel_Reg_Form.Name = "panel_Reg_Form";
            this.panel_Reg_Form.Size = new System.Drawing.Size(728, 416);
            this.panel_Reg_Form.TabIndex = 13;
            // 
            // lbl_Reg_Title
            // 
            this.lbl_Reg_Title.AutoSize = true;
            this.lbl_Reg_Title.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Reg_Title.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_Reg_Title.Location = new System.Drawing.Point(266, 17);
            this.lbl_Reg_Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Reg_Title.Name = "lbl_Reg_Title";
            this.lbl_Reg_Title.Size = new System.Drawing.Size(197, 35);
            this.lbl_Reg_Title.TabIndex = 11;
            this.lbl_Reg_Title.Text = "Registration Form";
            // 
            // rich_txt_box_address
            // 
            this.rich_txt_box_address.Location = new System.Drawing.Point(141, 166);
            this.rich_txt_box_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rich_txt_box_address.Name = "rich_txt_box_address";
            this.rich_txt_box_address.Size = new System.Drawing.Size(219, 88);
            this.rich_txt_box_address.TabIndex = 9;
            this.rich_txt_box_address.Text = "";
            // 
            // BtnRegister
            // 
            this.BtnRegister.BackColor = System.Drawing.Color.Aqua;
            this.BtnRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnRegister.Location = new System.Drawing.Point(303, 300);
            this.BtnRegister.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnRegister.Name = "BtnRegister";
            this.BtnRegister.Size = new System.Drawing.Size(101, 39);
            this.BtnRegister.TabIndex = 10;
            this.BtnRegister.Text = "Register";
            this.BtnRegister.UseVisualStyleBackColor = false;
            // 
            // msk_txt_box_pass
            // 
            this.msk_txt_box_pass.Location = new System.Drawing.Point(487, 64);
            this.msk_txt_box_pass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.msk_txt_box_pass.Name = "msk_txt_box_pass";
            this.msk_txt_box_pass.Size = new System.Drawing.Size(152, 22);
            this.msk_txt_box_pass.TabIndex = 6;
            this.msk_txt_box_pass.UseSystemPasswordChar = true;
            // 
            // txt_box_lname
            // 
            this.txt_box_lname.Location = new System.Drawing.Point(487, 113);
            this.txt_box_lname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_lname.Name = "txt_box_lname";
            this.txt_box_lname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_lname.TabIndex = 8;
            // 
            // txt_box_fname
            // 
            this.txt_box_fname.Location = new System.Drawing.Point(141, 111);
            this.txt_box_fname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_fname.Name = "txt_box_fname";
            this.txt_box_fname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_fname.TabIndex = 7;
            // 
            // txt_box_usname
            // 
            this.txt_box_usname.Location = new System.Drawing.Point(141, 64);
            this.txt_box_usname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_usname.Name = "txt_box_usname";
            this.txt_box_usname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_usname.TabIndex = 5;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Location = new System.Drawing.Point(45, 169);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(60, 17);
            this.lbl_address.TabIndex = 4;
            this.lbl_address.Text = "Address";
            // 
            // lbl_lname
            // 
            this.lbl_lname.AutoSize = true;
            this.lbl_lname.Location = new System.Drawing.Point(391, 116);
            this.lbl_lname.Name = "lbl_lname";
            this.lbl_lname.Size = new System.Drawing.Size(76, 17);
            this.lbl_lname.TabIndex = 3;
            this.lbl_lname.Text = "Last Name";
            // 
            // lbl_fname
            // 
            this.lbl_fname.AutoSize = true;
            this.lbl_fname.Location = new System.Drawing.Point(45, 113);
            this.lbl_fname.Name = "lbl_fname";
            this.lbl_fname.Size = new System.Drawing.Size(76, 17);
            this.lbl_fname.TabIndex = 2;
            this.lbl_fname.Text = "First Name";
            // 
            // lbl_pass
            // 
            this.lbl_pass.AutoSize = true;
            this.lbl_pass.Location = new System.Drawing.Point(397, 66);
            this.lbl_pass.Name = "lbl_pass";
            this.lbl_pass.Size = new System.Drawing.Size(69, 17);
            this.lbl_pass.TabIndex = 1;
            this.lbl_pass.Text = "Password";
            // 
            // lbl_uname
            // 
            this.lbl_uname.AutoSize = true;
            this.lbl_uname.Location = new System.Drawing.Point(45, 66);
            this.lbl_uname.Name = "lbl_uname";
            this.lbl_uname.Size = new System.Drawing.Size(73, 17);
            this.lbl_uname.TabIndex = 0;
            this.lbl_uname.Text = "Username";
            // 
            // tab_Retrieve
            // 
            this.tab_Retrieve.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Retrieve.Location = new System.Drawing.Point(4, 27);
            this.tab_Retrieve.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Retrieve.Name = "tab_Retrieve";
            this.tab_Retrieve.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Retrieve.Size = new System.Drawing.Size(731, 417);
            this.tab_Retrieve.TabIndex = 1;
            this.tab_Retrieve.Text = "Retrieval";
            // 
            // tab_Update
            // 
            this.tab_Update.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Update.Location = new System.Drawing.Point(4, 27);
            this.tab_Update.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Update.Name = "tab_Update";
            this.tab_Update.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Update.Size = new System.Drawing.Size(731, 417);
            this.tab_Update.TabIndex = 2;
            this.tab_Update.Text = "Update";
            // 
            // Tab_Delete
            // 
            this.Tab_Delete.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Tab_Delete.Location = new System.Drawing.Point(4, 27);
            this.Tab_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.Tab_Delete.Name = "Tab_Delete";
            this.Tab_Delete.Padding = new System.Windows.Forms.Padding(4);
            this.Tab_Delete.Size = new System.Drawing.Size(731, 417);
            this.Tab_Delete.TabIndex = 3;
            this.Tab_Delete.Text = "Delete";
            // 
            // FormCrud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1090, 670);
            this.Controls.Add(this.panel_crud_tabs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormCrud";
            this.Text = "Registration Form";
            this.panel_crud_tabs.ResumeLayout(false);
            this.tabCtrl_crud.ResumeLayout(false);
            this.tab_Reg.ResumeLayout(false);
            this.panel_Reg_Form.ResumeLayout(false);
            this.panel_Reg_Form.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_crud_tabs;
        private System.Windows.Forms.TabControl tabCtrl_crud;
        private System.Windows.Forms.TabPage tab_Reg;
        private System.Windows.Forms.Panel panel_Reg_Form;
        private System.Windows.Forms.Label lbl_Reg_Title;
        private System.Windows.Forms.RichTextBox rich_txt_box_address;
        private System.Windows.Forms.Button BtnRegister;
        private System.Windows.Forms.MaskedTextBox msk_txt_box_pass;
        private System.Windows.Forms.TextBox txt_box_lname;
        private System.Windows.Forms.TextBox txt_box_fname;
        private System.Windows.Forms.TextBox txt_box_usname;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_lname;
        private System.Windows.Forms.Label lbl_fname;
        private System.Windows.Forms.Label lbl_pass;
        private System.Windows.Forms.Label lbl_uname;
        private System.Windows.Forms.TabPage tab_Retrieve;
        private System.Windows.Forms.TabPage tab_Update;
        private System.Windows.Forms.TabPage Tab_Delete;
    }
}

