﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassEncrypt
{
    public class AbzsCrypt
    {
        private static string mySalt;
        AbzsCrypt(String salt)
        {
            mySalt = "@Abzs123";
        }

        // Encryption Algorithm
        static string Encrypt(StringBuilder passText)
        {
            for (int i = 0, j = 0; i < passText.Length && j <= mySalt.Length; i++, j++)
            {
                if (j == mySalt.Length) j = 0;
                Console.WriteLine((int)passText[i] + (int)mySalt[j]);
            }
            Console.WriteLine(passText);
            return passText.ToString();
        }

        // Decryption Algorithm
        string Decrypt(StringBuilder passHash)
        {
            for (int i = 0, j = 0; i < passHash.Length && j <= mySalt.Length; i++, j++)
            {
                if (j == mySalt.Length) j = 0;
                Console.WriteLine((int)passHash[i] - (int)mySalt[j]);
            }

            return passHash.ToString();
        }

        //Just for checking, would remove later, & it would work by using objects, static class would also be removed.
        static void main(string[] args)
        {
            Console.WriteLine(AbzsCrypt.Encrypt(new StringBuilder("hey")));
        }
    }
}
