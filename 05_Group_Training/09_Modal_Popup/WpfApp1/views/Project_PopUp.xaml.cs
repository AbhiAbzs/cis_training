﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1.views
{
    /// <summary>
    /// Interaction logic for Project_PopUp.xaml
    /// </summary>
    public partial class Project_PopUp : UserControl
    {
        public Project_PopUp()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow winObj = (MainWindow)Window.GetWindow(this);
            winObj.ChangeVisiblity();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow winObj = (MainWindow)Window.GetWindow(this);
            winObj.ChangeVisiblity();
        }
    }
}
