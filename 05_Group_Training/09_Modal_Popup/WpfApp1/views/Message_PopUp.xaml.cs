﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1;
using WpfApp1.views;

namespace WpfApp1.views
{
    /// <summary>
    /// Interaction logic for Message_PopUp.xaml
    /// </summary>
    public partial class Message_PopUp : UserControl
    {
        public Message_PopUp()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow winObj= (MainWindow) Window.GetWindow(this);
            winObj.ChangeVisiblity();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow winObj = (MainWindow)Window.GetWindow(this);
            winObj.ChangeVisiblity();
        }
    }
}
