﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace DemoDesktopWPFApp.Models
{
    class CityMod
    {
        string conStr = "Data Source=sql12-16.mt.cisinlive.com\\sql2012;Initial Catalog=dnntest;User ID=atul;Password=cis1234";
        List<string> cities = new List<string>();

        //Member Variables for SqlRelated Operations
        SqlConnection conObj;
        SqlCommand cmd;
        SqlDataReader dataReader;
        private int _stateID;

        public CityMod(int idStateId)
        {
            this._stateID = idStateId;
            conObj = new SqlConnection(conStr);
        }

        ~CityMod()
        {
            conObj.Close();
        }


        // Method for fetching States List from DB
        public List<string> GetCity()
        {
            string querStr = "SELECT City FROM Abzs_City WHERE StateId = " + @_stateID;
            conObj.Open();
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                do
                {
                    cities.Add(dataReader["City"].ToString().Trim());
                } while (dataReader.Read());
            }
            else
            {
                MessageBox.Show("There is no record of Cities available for this particular State in our Database.");
            }
            conObj.Close();
            return cities;
        }
    }
}
