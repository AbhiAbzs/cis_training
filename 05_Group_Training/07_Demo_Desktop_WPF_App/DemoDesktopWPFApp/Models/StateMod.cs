﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace DemoDesktopWPFApp.Models
{
    class StateMod
    {
        string conStr = "Data Source=sql12-16.mt.cisinlive.com\\sql2012;Initial Catalog=dnntest;User ID=atul;Password=cis1234";

        //Member Variables for SqlRelated Operations
        SqlConnection conObj;
        SqlCommand cmd;
        SqlDataReader dataReader;
        List<string> states = new List<string>();

        public StateMod()
        {
            conObj = new SqlConnection(conStr);
        }

        ~StateMod()
        {
            conObj.Close();
        }


        // Method for fetching States List from DB
        public List<string> GetStates()
        {

            string querStr = "Select State FROM Abzs_State;";
            conObj.Open();
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                do
                {
                    states.Add(dataReader["State"].ToString());
                } while (dataReader.Read());
            }
            else
            {
                MessageBox.Show("There is no record of States available in our Database.");
            }
            conObj.Close();
            return states;
        }
    }
}
