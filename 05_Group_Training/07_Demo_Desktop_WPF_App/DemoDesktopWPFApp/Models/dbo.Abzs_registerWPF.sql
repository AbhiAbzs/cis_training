﻿CREATE TABLE [Atul].[Abzs_RegisterWPF] (
    [UserId]   INT         IDENTITY (1, 1) NOT NULL,
    [UserName] NCHAR (25)  NOT NULL UNIQUE,
    [Password] NCHAR (100) NOT NULL,
    [Email]    NCHAR (25)  NOT NULL,
    [Contact]  NCHAR (10)  NOT NULL,
    [Gender]   NCHAR (10)  NOT NULL,
    [DOB]      DATE        NOT NULL,
    [State]    NCHAR (25)  NOT NULL,
    [City]     NCHAR (25)  NOT NULL,
    [Address]  NCHAR (100) NULL,
    CONSTRAINT [PK_Abzs_RegisterWPF] PRIMARY KEY CLUSTERED ([UserId] ASC),
    UNIQUE NONCLUSTERED ([UserName] ASC)
);

