﻿using System.Windows;

namespace DemoDesktopWPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Login_Reg_Btn_Click(object sender, RoutedEventArgs e)
        {
            if (Login_Reg_Btn.Content.Equals("Register"))
            {
                Login_Reg_Btn.Content = "Login";
                LoginGrid_UsrCtrl.Visibility = Visibility.Collapsed;
                RegisterGrid_UsrCtrl.Visibility = Visibility.Visible;

            }
            else
            {
                Login_Reg_Btn.Content = "Register";
                RegisterGrid_UsrCtrl.Visibility = Visibility.Collapsed;
                LoginGrid_UsrCtrl.Visibility = Visibility.Visible;
            }
        }
    }
}
