﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DemoDesktopWPFApp;

namespace DemoDesktopWPFApp.Windows
{
    /// <summary>
    /// Interaction logic for ForgotPassWin.xaml
    /// </summary>
    public partial class ForgotPassWin : Window
    {
        public ForgotPassWin()
        {
            InitializeComponent();
        }

        private void ForgotPass_Window_Closed(object sender, EventArgs e)
        {
            MainWindow newWin = new MainWindow();
            newWin.Show();
        }

        private void ForgetPass_TxtBx_GotFocus(object sender, RoutedEventArgs e)
        {
            if (forgetPass_TxtBx.Text.Equals("Enter Email ID here..."))
            {
                forgetPass_TxtBx.Text = "";
            }
        }

        private void ForgetPass_TxtBx_LostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(forgetPass_TxtBx.Text))
            {
                forgetPass_TxtBx.Text = "Enter Email ID here...";
            }
        }
    }
}
