﻿#pragma checksum "..\..\..\UserControls\Profile.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9F89C670177354C8AB102F219AC15F593020F475"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DemoDesktopWPFApp.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DemoDesktopWPFApp.UserControls {
    
    
    /// <summary>
    /// Profile
    /// </summary>
    public partial class Profile : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 7 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DemoDesktopWPFApp.UserControls.Profile ProfileUsrCtrl;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Profile_Grid;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ProfileTitle;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox User_Name_txtbx_Pro;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox passBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox conf_PassBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Update_Pro_Btn;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Delete_Btn;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label usrName_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label pass_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label rePass_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox email_txtbx_Pro;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label email_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label mobNo_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label dob_label_Pro;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MobNo_txtbx_Pro;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dob_Picker_Pro;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gender_GroupBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gender_grid;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton male_radioBtn_Pro;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton female_RadioBtn_Pro;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox address_GroupBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid address_Grid_Pro;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RichTextBox address_RichTxtBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox CityState_GroupBx_Pro;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid state_City_Grid_Pro;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox state_ComboBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox city_ComboBox_Pro;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\UserControls\Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editProfile;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DemoDesktopWPFApp;component/usercontrols/profile.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\Profile.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ProfileUsrCtrl = ((DemoDesktopWPFApp.UserControls.Profile)(target));
            return;
            case 2:
            this.Profile_Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.ProfileTitle = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.User_Name_txtbx_Pro = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.passBox_Pro = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 6:
            this.conf_PassBox_Pro = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 7:
            this.Update_Pro_Btn = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.Delete_Btn = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.usrName_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.pass_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.rePass_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.email_txtbx_Pro = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.email_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.mobNo_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.dob_label_Pro = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.MobNo_txtbx_Pro = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.dob_Picker_Pro = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 18:
            this.gender_GroupBox_Pro = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 19:
            this.gender_grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 20:
            this.male_radioBtn_Pro = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 21:
            this.female_RadioBtn_Pro = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 22:
            this.address_GroupBox_Pro = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 23:
            this.address_Grid_Pro = ((System.Windows.Controls.Grid)(target));
            return;
            case 24:
            this.address_RichTxtBox_Pro = ((System.Windows.Controls.RichTextBox)(target));
            return;
            case 25:
            this.CityState_GroupBx_Pro = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 26:
            this.state_City_Grid_Pro = ((System.Windows.Controls.Grid)(target));
            return;
            case 27:
            this.state_ComboBox_Pro = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 28:
            this.city_ComboBox_Pro = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 29:
            this.editProfile = ((System.Windows.Controls.Button)(target));
            
            #line 153 "..\..\..\UserControls\Profile.xaml"
            this.editProfile.Click += new System.Windows.RoutedEventHandler(this.EditProfile_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

