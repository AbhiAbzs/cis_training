﻿using DemoDesktopWPFApp.Windows;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DemoDesktopWPFApp.UserControls
{
    /// <summary>
    /// Interaction logic for Login_UserControl.xaml
    /// </summary>
    public partial class Login_UserControl : UserControl
    {
        public Login_UserControl()
        {
            InitializeComponent();
        }

        private void User_Name_txtbx_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (User_Name_txtbx.Text.Equals("Enter User Name here..."))
                User_Name_txtbx.Text = "";
        }

        private void User_Name_txtbx_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(User_Name_txtbx.Text))
                User_Name_txtbx.Text = "Enter User Name here...";
        }

        private void ForgotPass_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (forgotPass_Btn.Foreground.ToString() == Colors.Black.ToString())
            {
                forgotPass_Btn.Foreground = new SolidColorBrush(Colors.Blue);
            }
        }

        private void ForgotPass_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (forgotPass_Btn.Foreground.ToString() == Colors.Blue.ToString())
            {
                forgotPass_Btn.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void PassBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(passBox.Password))
                passBox.Password = "";
        }

        private void PassBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(passBox.Password))
                passBox.Password = "        ";
        }
        private void ForgotPass_Btn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ForgotPassWin fpassWin = new ForgotPassWin();
            fpassWin.Show();
            Window.GetWindow(this).Close();
        }

        private void Login_Btn_Click(object sender, RoutedEventArgs e)
        {
            // check user name and password from Db, if authorized then open dashboard window & Show user profile
            // Pass username while opening dashboard, so that that users profile can be displayed
            bool authorized = false;

            bool isAuthorized()
            {
                return true;    //for the time being, to test, dashboard window
            }

            authorized = isAuthorized();
            if (authorized)
            {
                Window DashboardWinObj = new Dashboard();
                DashboardWinObj.Show();
                Window.GetWindow(this).Close();
            }
        }
    }
}
