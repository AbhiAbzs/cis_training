﻿using System.Windows;
using System.Windows.Controls;

namespace DemoDesktopWPFApp.UserControls
{
    /// <summary>
    /// Interaction logic for Profile.xaml
    /// </summary>
    public partial class Profile : UserControl
    {
        private bool enabled = false;

        public Profile()
        {
            InitializeComponent();
        }

        public bool Enabled { get => enabled; set => enabled = value; }

        private void EditProfile_Click(object sender, RoutedEventArgs e)
        {
            if (Enabled)
            {
                User_Name_txtbx_Pro.IsEnabled = passBox_Pro.IsEnabled = conf_PassBox_Pro.IsEnabled =
                email_txtbx_Pro.IsEnabled = MobNo_txtbx_Pro.IsEnabled = dob_label_Pro.IsEnabled =
                    male_radioBtn_Pro.IsEnabled = female_RadioBtn_Pro.IsEnabled =
                        state_ComboBox_Pro.IsEnabled = city_ComboBox_Pro.IsEnabled =
                            address_RichTxtBox_Pro.IsEnabled = false;
            }
            else
            {
                User_Name_txtbx_Pro.IsEnabled = passBox_Pro.IsEnabled = conf_PassBox_Pro.IsEnabled =
                    email_txtbx_Pro.IsEnabled = MobNo_txtbx_Pro.IsEnabled = dob_label_Pro.IsEnabled =
                        male_radioBtn_Pro.IsEnabled = female_RadioBtn_Pro.IsEnabled =
                            state_ComboBox_Pro.IsEnabled = city_ComboBox_Pro.IsEnabled =
                                address_RichTxtBox_Pro.IsEnabled = true;
            }
            Enabled = !Enabled;
        }
    }
}
