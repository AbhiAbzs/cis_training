﻿using DemoDesktopWPFApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DemoDesktopWPFApp.UserControls
{
    /// <summary>
    /// Interaction logic for Register_UserControlObj.xaml
    /// </summary>
    public partial class Register_UserControlObj : UserControl
    {
        private Dictionary<String, bool> isValidDictionary = new Dictionary<string, bool>();
        public Register_UserControlObj()
        {
            InitializeComponent();
            dob_Picker_Reg.Text = DateTime.Today.ToShortDateString();
            AddStates();
        }

        private void User_Name_txtbx_Reg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (User_Name_txtbx_Reg.Text.Equals("Enter User Name here..."))
                User_Name_txtbx_Reg.Text = "";
        }

        private void User_Name_txtbx_Reg_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(User_Name_txtbx_Reg.Text))
                User_Name_txtbx_Reg.Text = "Enter User Name here...";
        }

        private void PassBox_Reg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(passBox_Reg.Password))
                passBox_Reg.Password = "";
        }

        private void PassBox_Reg_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(passBox_Reg.Password))
                passBox_Reg.Password = "        ";
        }

        private void Conf_PassBox_Reg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(conf_PassBox_Reg.Password))
                conf_PassBox_Reg.Password = "";
        }

        private void Conf_PassBox_Reg_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(conf_PassBox_Reg.Password))
                conf_PassBox_Reg.Password = "        ";
        }

        private void Email_txtbx_Reg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (email_txtbx_Reg.Text.Equals("Enter Email ID..."))
                email_txtbx_Reg.Text = "";
        }

        private void Email_txtbx_Reg_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(email_txtbx_Reg.Text))
                email_txtbx_Reg.Text = "Enter Email ID...";
        }

        private void MobNo_txtbx_Reg_GotFocus(object sender, RoutedEventArgs e)
        {
            if (MobNo_txtbx_Reg.Text.Equals("Enter Mobile Number..."))
                MobNo_txtbx_Reg.Text = "";
        }

        private void MobNo_txtbx_Reg_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(MobNo_txtbx_Reg.Text))
                MobNo_txtbx_Reg.Text = "Enter Mobile Number...";
        }

        private void Reset_Fields_Btn_Click(object sender, RoutedEventArgs e)
        {
            User_Name_txtbx_Reg.Text = "Enter User Name here...";
            passBox_Reg.Password = "        ";
            conf_PassBox_Reg.Password = "        ";
            email_txtbx_Reg.Text = "Enter Email ID...";
            MobNo_txtbx_Reg.Text = "Enter Mobile Number...";
            male_radioBtn.IsChecked = false;
            female_RadioBtn.IsChecked = false;
            address_RichTxtBox.Document.Blocks.Clear();
            dob_Picker_Reg.Text = DateTime.Today.ToShortDateString();
            state_ComboBox.SelectedIndex = 0;
            city_ComboBox.SelectedIndex = 0;
        }

        private void Register_Btn_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = Validate();

            if (isValid)
            {
                MessageBox.Show("Welcome!!!\nThanks for Registering.");
                // Resetting and redirecting the user to Login screen (hiding registration screen visiblity and showing login screen).
                MainWindow obj = (MainWindow)MainWindow.GetWindow(this);
                if (obj.Login_Reg_Btn.Content.Equals("Register"))
                {
                    obj.Login_Reg_Btn.Content = "Login";
                    obj.LoginGrid_UsrCtrl.Visibility = Visibility.Collapsed;
                    obj.RegisterGrid_UsrCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    obj.Login_Reg_Btn.Content = "Register";
                    obj.RegisterGrid_UsrCtrl.Visibility = Visibility.Collapsed;
                    obj.LoginGrid_UsrCtrl.Visibility = Visibility.Visible;
                }
                // Database entry of all the data inserted by the user
            }
            else
            {
                MessageBox.Show("Validation Error, Insert Proper Data.");
            }
        }

        void AddStates()
        {
            List<string> states = new StateMod().GetStates();
            foreach (string state in states)
            {
                state_ComboBox.Items.Add(state.Trim());
            }
        }

        private void State_ComboBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            city_ComboBox.Items.Clear();
            city_ComboBox.Items.Add("City");
            city_ComboBox.Text = "City";
        }

        void PopulateCity(int Id)
        {
            List<string> cities = new CityMod(Id).GetCity();
            foreach (string city in cities)
            {
                city_ComboBox.Items.Add(city);
            }

        }

        private void City_ComboBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (state_ComboBox.SelectedItem.Equals("State"))
            {
                MessageBox.Show("Please Select your State first.");
            }
            else
            {
                if (city_ComboBox.Items.Count <= 1)
                {
                    PopulateCity(state_ComboBox.SelectedIndex);
                }
            }
        }

        // Logic for validation of the form before submitting (inserting into DB)
        private bool Validate()
        {
            bool valid = false;

            
            return valid;
        }

    }
}
