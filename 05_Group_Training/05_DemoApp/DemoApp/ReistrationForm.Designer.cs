﻿namespace DemoApp
{
    partial class FormCrud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
    private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCrud));
            this.panel_crud_tabs = new System.Windows.Forms.Panel();
            this.tabCtrl_crud = new System.Windows.Forms.TabControl();
            this.tab_Reg = new System.Windows.Forms.TabPage();
            this.panel_Reg_Form = new System.Windows.Forms.Panel();
            this.btn_Reset_Reg = new System.Windows.Forms.Button();
            this.lbl_Reg_Title = new System.Windows.Forms.Label();
            this.rich_txt_box_address = new System.Windows.Forms.RichTextBox();
            this.BtnRegister = new System.Windows.Forms.Button();
            this.msk_txt_box_pass = new System.Windows.Forms.MaskedTextBox();
            this.txt_box_lname = new System.Windows.Forms.TextBox();
            this.txt_box_fname = new System.Windows.Forms.TextBox();
            this.txt_box_usname = new System.Windows.Forms.TextBox();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_lname = new System.Windows.Forms.Label();
            this.lbl_fname = new System.Windows.Forms.Label();
            this.lbl_pass = new System.Windows.Forms.Label();
            this.lbl_uname = new System.Windows.Forms.Label();
            this.tab_Retrieve = new System.Windows.Forms.TabPage();
            this.Panel_Retrival = new System.Windows.Forms.Panel();
            this.lstbox_Retrieval = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Btn_All_Retrieval = new System.Windows.Forms.Button();
            this.Lbl_Title_Retrieve = new System.Windows.Forms.Label();
            this.Btn_Search_Retrieval = new System.Windows.Forms.Button();
            this.Txtbox_Search_Retrieval = new System.Windows.Forms.TextBox();
            this.tab_Update = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_Search_Updation = new System.Windows.Forms.Button();
            this.Txtbox_Search_Updation = new System.Windows.Forms.TextBox();
            this.pnl_Updation_Form = new System.Windows.Forms.Panel();
            this.btn_Reset_Updation = new System.Windows.Forms.Button();
            this.rich_Txtbox_Address_Updation = new System.Windows.Forms.RichTextBox();
            this.btn_Update = new System.Windows.Forms.Button();
            this.msk_txtbox_Pass_Updation = new System.Windows.Forms.MaskedTextBox();
            this.txtbox_Lname_Updation = new System.Windows.Forms.TextBox();
            this.txtbox_Fname_Updation = new System.Windows.Forms.TextBox();
            this.txtbox_Uname_Updation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_Updation = new System.Windows.Forms.Label();
            this.Tab_Delete = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_Col_Header_Del = new System.Windows.Forms.Label();
            this.chkd_LstBx_Del = new System.Windows.Forms.CheckedListBox();
            this.lbl_Uid_Del = new System.Windows.Forms.Label();
            this.btn_MarkAll_Del = new System.Windows.Forms.Button();
            this.btn_Del = new System.Windows.Forms.Button();
            this.btn_ViewAll_Del = new System.Windows.Forms.Button();
            this.lbl_Del = new System.Windows.Forms.Label();
            this.btn_Search_Del = new System.Windows.Forms.Button();
            this.Txtbox_Search_Deletion = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabCtrl_crud.SuspendLayout();
            this.tab_Reg.SuspendLayout();
            this.panel_Reg_Form.SuspendLayout();
            this.tab_Retrieve.SuspendLayout();
            this.Panel_Retrival.SuspendLayout();
            this.tab_Update.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnl_Updation_Form.SuspendLayout();
            this.Tab_Delete.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_crud_tabs
            // 
            this.panel_crud_tabs.AutoSize = true;
            this.panel_crud_tabs.Location = new System.Drawing.Point(0, 32);
            this.panel_crud_tabs.Margin = new System.Windows.Forms.Padding(4);
            this.panel_crud_tabs.Name = "panel_crud_tabs";
            this.panel_crud_tabs.Size = new System.Drawing.Size(735, 444);
            this.panel_crud_tabs.TabIndex = 13;
            // 
            // tabCtrl_crud
            // 
            this.tabCtrl_crud.Controls.Add(this.tab_Reg);
            this.tabCtrl_crud.Controls.Add(this.tab_Retrieve);
            this.tabCtrl_crud.Controls.Add(this.tab_Update);
            this.tabCtrl_crud.Controls.Add(this.Tab_Delete);
            this.tabCtrl_crud.Location = new System.Drawing.Point(0, 32);
            this.tabCtrl_crud.Margin = new System.Windows.Forms.Padding(4);
            this.tabCtrl_crud.Name = "tabCtrl_crud";
            this.tabCtrl_crud.Padding = new System.Drawing.Point(6, 4);
            this.tabCtrl_crud.SelectedIndex = 0;
            this.tabCtrl_crud.Size = new System.Drawing.Size(735, 448);
            this.tabCtrl_crud.TabIndex = 0;
            // 
            // tab_Reg
            // 
            this.tab_Reg.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Reg.Controls.Add(this.panel_Reg_Form);
            this.tab_Reg.Location = new System.Drawing.Point(4, 27);
            this.tab_Reg.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Reg.Name = "tab_Reg";
            this.tab_Reg.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Reg.Size = new System.Drawing.Size(727, 417);
            this.tab_Reg.TabIndex = 0;
            this.tab_Reg.Tag = "";
            this.tab_Reg.Text = "Register/Creation";
            // 
            // panel_Reg_Form
            // 
            this.panel_Reg_Form.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel_Reg_Form.Controls.Add(this.btn_Reset_Reg);
            this.panel_Reg_Form.Controls.Add(this.lbl_Reg_Title);
            this.panel_Reg_Form.Controls.Add(this.rich_txt_box_address);
            this.panel_Reg_Form.Controls.Add(this.BtnRegister);
            this.panel_Reg_Form.Controls.Add(this.msk_txt_box_pass);
            this.panel_Reg_Form.Controls.Add(this.txt_box_lname);
            this.panel_Reg_Form.Controls.Add(this.txt_box_fname);
            this.panel_Reg_Form.Controls.Add(this.txt_box_usname);
            this.panel_Reg_Form.Controls.Add(this.lbl_address);
            this.panel_Reg_Form.Controls.Add(this.lbl_lname);
            this.panel_Reg_Form.Controls.Add(this.lbl_fname);
            this.panel_Reg_Form.Controls.Add(this.lbl_pass);
            this.panel_Reg_Form.Controls.Add(this.lbl_uname);
            this.panel_Reg_Form.Location = new System.Drawing.Point(0, 0);
            this.panel_Reg_Form.Margin = new System.Windows.Forms.Padding(4);
            this.panel_Reg_Form.Name = "panel_Reg_Form";
            this.panel_Reg_Form.Size = new System.Drawing.Size(728, 416);
            this.panel_Reg_Form.TabIndex = 13;
            // 
            // btn_Reset_Reg
            // 
            this.btn_Reset_Reg.BackColor = System.Drawing.Color.Coral;
            this.btn_Reset_Reg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Reset_Reg.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Reset_Reg.Location = new System.Drawing.Point(396, 317);
            this.btn_Reset_Reg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Reset_Reg.Name = "btn_Reset_Reg";
            this.btn_Reset_Reg.Size = new System.Drawing.Size(101, 39);
            this.btn_Reset_Reg.TabIndex = 11;
            this.btn_Reset_Reg.Text = "Reset";
            this.btn_Reset_Reg.UseVisualStyleBackColor = false;
            this.btn_Reset_Reg.Click += new System.EventHandler(this.btn_Reset_Reg_Click);
            // 
            // lbl_Reg_Title
            // 
            this.lbl_Reg_Title.AutoSize = true;
            this.lbl_Reg_Title.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Reg_Title.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_Reg_Title.Location = new System.Drawing.Point(266, 17);
            this.lbl_Reg_Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Reg_Title.Name = "lbl_Reg_Title";
            this.lbl_Reg_Title.Size = new System.Drawing.Size(196, 35);
            this.lbl_Reg_Title.TabIndex = 1;
            this.lbl_Reg_Title.Text = "Registration Form";
            // 
            // rich_txt_box_address
            // 
            this.rich_txt_box_address.Location = new System.Drawing.Point(154, 181);
            this.rich_txt_box_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rich_txt_box_address.Name = "rich_txt_box_address";
            this.rich_txt_box_address.Size = new System.Drawing.Size(219, 88);
            this.rich_txt_box_address.TabIndex = 9;
            this.rich_txt_box_address.Text = "";
            // 
            // BtnRegister
            // 
            this.BtnRegister.BackColor = System.Drawing.Color.Aqua;
            this.BtnRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnRegister.Location = new System.Drawing.Point(254, 317);
            this.BtnRegister.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnRegister.Name = "BtnRegister";
            this.BtnRegister.Size = new System.Drawing.Size(101, 39);
            this.BtnRegister.TabIndex = 10;
            this.BtnRegister.Text = "Register";
            this.BtnRegister.UseVisualStyleBackColor = false;
            this.BtnRegister.Click += new System.EventHandler(this.BtnRegister_Click);
            // 
            // msk_txt_box_pass
            // 
            this.msk_txt_box_pass.Location = new System.Drawing.Point(500, 79);
            this.msk_txt_box_pass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.msk_txt_box_pass.Name = "msk_txt_box_pass";
            this.msk_txt_box_pass.Size = new System.Drawing.Size(152, 22);
            this.msk_txt_box_pass.TabIndex = 6;
            this.msk_txt_box_pass.UseSystemPasswordChar = true;
            // 
            // txt_box_lname
            // 
            this.txt_box_lname.Location = new System.Drawing.Point(500, 128);
            this.txt_box_lname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_lname.Name = "txt_box_lname";
            this.txt_box_lname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_lname.TabIndex = 8;
            // 
            // txt_box_fname
            // 
            this.txt_box_fname.Location = new System.Drawing.Point(154, 126);
            this.txt_box_fname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_fname.Name = "txt_box_fname";
            this.txt_box_fname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_fname.TabIndex = 7;
            // 
            // txt_box_usname
            // 
            this.txt_box_usname.Location = new System.Drawing.Point(154, 79);
            this.txt_box_usname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_box_usname.Name = "txt_box_usname";
            this.txt_box_usname.Size = new System.Drawing.Size(152, 22);
            this.txt_box_usname.TabIndex = 5;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Location = new System.Drawing.Point(58, 184);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(60, 17);
            this.lbl_address.TabIndex = 5;
            this.lbl_address.Text = "Address";
            // 
            // lbl_lname
            // 
            this.lbl_lname.AutoSize = true;
            this.lbl_lname.Location = new System.Drawing.Point(404, 131);
            this.lbl_lname.Name = "lbl_lname";
            this.lbl_lname.Size = new System.Drawing.Size(76, 17);
            this.lbl_lname.TabIndex = 4;
            this.lbl_lname.Text = "Last Name";
            // 
            // lbl_fname
            // 
            this.lbl_fname.AutoSize = true;
            this.lbl_fname.Location = new System.Drawing.Point(58, 128);
            this.lbl_fname.Name = "lbl_fname";
            this.lbl_fname.Size = new System.Drawing.Size(76, 17);
            this.lbl_fname.TabIndex = 3;
            this.lbl_fname.Text = "First Name";
            // 
            // lbl_pass
            // 
            this.lbl_pass.AutoSize = true;
            this.lbl_pass.Location = new System.Drawing.Point(410, 81);
            this.lbl_pass.Name = "lbl_pass";
            this.lbl_pass.Size = new System.Drawing.Size(69, 17);
            this.lbl_pass.TabIndex = 2;
            this.lbl_pass.Text = "Password";
            // 
            // lbl_uname
            // 
            this.lbl_uname.AutoSize = true;
            this.lbl_uname.Location = new System.Drawing.Point(58, 81);
            this.lbl_uname.Name = "lbl_uname";
            this.lbl_uname.Size = new System.Drawing.Size(73, 17);
            this.lbl_uname.TabIndex = 1;
            this.lbl_uname.Text = "Username";
            // 
            // tab_Retrieve
            // 
            this.tab_Retrieve.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Retrieve.Controls.Add(this.Panel_Retrival);
            this.tab_Retrieve.Location = new System.Drawing.Point(4, 27);
            this.tab_Retrieve.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Retrieve.Name = "tab_Retrieve";
            this.tab_Retrieve.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Retrieve.Size = new System.Drawing.Size(727, 417);
            this.tab_Retrieve.TabIndex = 1;
            this.tab_Retrieve.Text = "Retrieval";
            // 
            // Panel_Retrival
            // 
            this.Panel_Retrival.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Panel_Retrival.Controls.Add(this.lstbox_Retrieval);
            this.Panel_Retrival.Controls.Add(this.label9);
            this.Panel_Retrival.Controls.Add(this.Btn_All_Retrieval);
            this.Panel_Retrival.Controls.Add(this.Lbl_Title_Retrieve);
            this.Panel_Retrival.Controls.Add(this.Btn_Search_Retrieval);
            this.Panel_Retrival.Controls.Add(this.Txtbox_Search_Retrieval);
            this.Panel_Retrival.Location = new System.Drawing.Point(1, 0);
            this.Panel_Retrival.Margin = new System.Windows.Forms.Padding(4);
            this.Panel_Retrival.Name = "Panel_Retrival";
            this.Panel_Retrival.Size = new System.Drawing.Size(728, 416);
            this.Panel_Retrival.TabIndex = 16;
            // 
            // lstbox_Retrieval
            // 
            this.lstbox_Retrieval.FormattingEnabled = true;
            this.lstbox_Retrieval.HorizontalScrollbar = true;
            this.lstbox_Retrieval.ImeMode = System.Windows.Forms.ImeMode.On;
            this.lstbox_Retrieval.ItemHeight = 16;
            this.lstbox_Retrieval.Location = new System.Drawing.Point(62, 132);
            this.lstbox_Retrieval.Name = "lstbox_Retrieval";
            this.lstbox_Retrieval.Size = new System.Drawing.Size(598, 244);
            this.lstbox_Retrieval.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(59, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Enter UID";
            // 
            // Btn_All_Retrieval
            // 
            this.Btn_All_Retrieval.BackColor = System.Drawing.Color.Aqua;
            this.Btn_All_Retrieval.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_All_Retrieval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_All_Retrieval.Location = new System.Drawing.Point(559, 76);
            this.Btn_All_Retrieval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_All_Retrieval.Name = "Btn_All_Retrieval";
            this.Btn_All_Retrieval.Size = new System.Drawing.Size(101, 39);
            this.Btn_All_Retrieval.TabIndex = 12;
            this.Btn_All_Retrieval.Text = "View All";
            this.Btn_All_Retrieval.UseVisualStyleBackColor = false;
            this.Btn_All_Retrieval.Click += new System.EventHandler(this.Btn_All_Retrieval_Click);
            // 
            // Lbl_Title_Retrieve
            // 
            this.Lbl_Title_Retrieve.AutoSize = true;
            this.Lbl_Title_Retrieve.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Title_Retrieve.ForeColor = System.Drawing.Color.Maroon;
            this.Lbl_Title_Retrieve.Location = new System.Drawing.Point(282, 18);
            this.Lbl_Title_Retrieve.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_Title_Retrieve.Name = "Lbl_Title_Retrieve";
            this.Lbl_Title_Retrieve.Size = new System.Drawing.Size(165, 35);
            this.Lbl_Title_Retrieve.TabIndex = 11;
            this.Lbl_Title_Retrieve.Text = "Retrieval Form";
            // 
            // Btn_Search_Retrieval
            // 
            this.Btn_Search_Retrieval.BackColor = System.Drawing.Color.Aqua;
            this.Btn_Search_Retrieval.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Search_Retrieval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Search_Retrieval.Location = new System.Drawing.Point(434, 76);
            this.Btn_Search_Retrieval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_Search_Retrieval.Name = "Btn_Search_Retrieval";
            this.Btn_Search_Retrieval.Size = new System.Drawing.Size(101, 39);
            this.Btn_Search_Retrieval.TabIndex = 10;
            this.Btn_Search_Retrieval.Text = "Search";
            this.Btn_Search_Retrieval.UseVisualStyleBackColor = false;
            this.Btn_Search_Retrieval.Click += new System.EventHandler(this.Btn_Search_Retrieval_Click);
            // 
            // Txtbox_Search_Retrieval
            // 
            this.Txtbox_Search_Retrieval.Location = new System.Drawing.Point(140, 84);
            this.Txtbox_Search_Retrieval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txtbox_Search_Retrieval.Name = "Txtbox_Search_Retrieval";
            this.Txtbox_Search_Retrieval.Size = new System.Drawing.Size(282, 22);
            this.Txtbox_Search_Retrieval.TabIndex = 5;
            // 
            // tab_Update
            // 
            this.tab_Update.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tab_Update.Controls.Add(this.panel1);
            this.tab_Update.Location = new System.Drawing.Point(4, 27);
            this.tab_Update.Margin = new System.Windows.Forms.Padding(4);
            this.tab_Update.Name = "tab_Update";
            this.tab_Update.Padding = new System.Windows.Forms.Padding(4);
            this.tab_Update.Size = new System.Drawing.Size(727, 417);
            this.tab_Update.TabIndex = 2;
            this.tab_Update.Text = "Update";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btn_Search_Updation);
            this.panel1.Controls.Add(this.Txtbox_Search_Updation);
            this.panel1.Controls.Add(this.pnl_Updation_Form);
            this.panel1.Controls.Add(this.lbl_Updation);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(728, 416);
            this.panel1.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(60, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Enter UID";
            // 
            // btn_Search_Updation
            // 
            this.btn_Search_Updation.BackColor = System.Drawing.Color.Aqua;
            this.btn_Search_Updation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Search_Updation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Search_Updation.Location = new System.Drawing.Point(453, 64);
            this.btn_Search_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Search_Updation.Name = "btn_Search_Updation";
            this.btn_Search_Updation.Size = new System.Drawing.Size(101, 39);
            this.btn_Search_Updation.TabIndex = 14;
            this.btn_Search_Updation.Text = "Search";
            this.btn_Search_Updation.UseVisualStyleBackColor = false;
            this.btn_Search_Updation.Click += new System.EventHandler(this.btn_Search_Updation_Click);
            // 
            // Txtbox_Search_Updation
            // 
            this.Txtbox_Search_Updation.Location = new System.Drawing.Point(156, 72);
            this.Txtbox_Search_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txtbox_Search_Updation.Name = "Txtbox_Search_Updation";
            this.Txtbox_Search_Updation.Size = new System.Drawing.Size(282, 22);
            this.Txtbox_Search_Updation.TabIndex = 13;
            // 
            // pnl_Updation_Form
            // 
            this.pnl_Updation_Form.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_Updation_Form.Controls.Add(this.btn_Reset_Updation);
            this.pnl_Updation_Form.Controls.Add(this.rich_Txtbox_Address_Updation);
            this.pnl_Updation_Form.Controls.Add(this.btn_Update);
            this.pnl_Updation_Form.Controls.Add(this.msk_txtbox_Pass_Updation);
            this.pnl_Updation_Form.Controls.Add(this.txtbox_Lname_Updation);
            this.pnl_Updation_Form.Controls.Add(this.txtbox_Fname_Updation);
            this.pnl_Updation_Form.Controls.Add(this.txtbox_Uname_Updation);
            this.pnl_Updation_Form.Controls.Add(this.label2);
            this.pnl_Updation_Form.Controls.Add(this.label3);
            this.pnl_Updation_Form.Controls.Add(this.label4);
            this.pnl_Updation_Form.Controls.Add(this.label5);
            this.pnl_Updation_Form.Controls.Add(this.label6);
            this.pnl_Updation_Form.Location = new System.Drawing.Point(33, 112);
            this.pnl_Updation_Form.Name = "pnl_Updation_Form";
            this.pnl_Updation_Form.Size = new System.Drawing.Size(665, 295);
            this.pnl_Updation_Form.TabIndex = 15;
            // 
            // btn_Reset_Updation
            // 
            this.btn_Reset_Updation.BackColor = System.Drawing.Color.Coral;
            this.btn_Reset_Updation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Reset_Updation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Reset_Updation.Location = new System.Drawing.Point(351, 236);
            this.btn_Reset_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Reset_Updation.Name = "btn_Reset_Updation";
            this.btn_Reset_Updation.Size = new System.Drawing.Size(101, 39);
            this.btn_Reset_Updation.TabIndex = 27;
            this.btn_Reset_Updation.Text = "Reset";
            this.btn_Reset_Updation.UseVisualStyleBackColor = false;
            this.btn_Reset_Updation.Click += new System.EventHandler(this.btn_Reset_Updation_Click);
            // 
            // rich_Txtbox_Address_Updation
            // 
            this.rich_Txtbox_Address_Updation.Location = new System.Drawing.Point(126, 115);
            this.rich_Txtbox_Address_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rich_Txtbox_Address_Updation.Name = "rich_Txtbox_Address_Updation";
            this.rich_Txtbox_Address_Updation.Size = new System.Drawing.Size(219, 88);
            this.rich_Txtbox_Address_Updation.TabIndex = 25;
            this.rich_Txtbox_Address_Updation.Text = "";
            // 
            // btn_Update
            // 
            this.btn_Update.BackColor = System.Drawing.Color.Aqua;
            this.btn_Update.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Update.Location = new System.Drawing.Point(196, 236);
            this.btn_Update.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(101, 39);
            this.btn_Update.TabIndex = 26;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = false;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // msk_txtbox_Pass_Updation
            // 
            this.msk_txtbox_Pass_Updation.Location = new System.Drawing.Point(472, 13);
            this.msk_txtbox_Pass_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.msk_txtbox_Pass_Updation.Name = "msk_txtbox_Pass_Updation";
            this.msk_txtbox_Pass_Updation.Size = new System.Drawing.Size(152, 22);
            this.msk_txtbox_Pass_Updation.TabIndex = 22;
            this.msk_txtbox_Pass_Updation.UseSystemPasswordChar = true;
            // 
            // txtbox_Lname_Updation
            // 
            this.txtbox_Lname_Updation.Location = new System.Drawing.Point(472, 62);
            this.txtbox_Lname_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Lname_Updation.Name = "txtbox_Lname_Updation";
            this.txtbox_Lname_Updation.Size = new System.Drawing.Size(152, 22);
            this.txtbox_Lname_Updation.TabIndex = 24;
            // 
            // txtbox_Fname_Updation
            // 
            this.txtbox_Fname_Updation.Location = new System.Drawing.Point(126, 60);
            this.txtbox_Fname_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Fname_Updation.Name = "txtbox_Fname_Updation";
            this.txtbox_Fname_Updation.Size = new System.Drawing.Size(152, 22);
            this.txtbox_Fname_Updation.TabIndex = 23;
            // 
            // txtbox_Uname_Updation
            // 
            this.txtbox_Uname_Updation.Location = new System.Drawing.Point(126, 13);
            this.txtbox_Uname_Updation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtbox_Uname_Updation.Name = "txtbox_Uname_Updation";
            this.txtbox_Uname_Updation.Size = new System.Drawing.Size(152, 22);
            this.txtbox_Uname_Updation.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(376, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Last Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "First Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(382, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 17;
            this.label5.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Username";
            // 
            // lbl_Updation
            // 
            this.lbl_Updation.AutoSize = true;
            this.lbl_Updation.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Updation.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_Updation.Location = new System.Drawing.Point(281, 17);
            this.lbl_Updation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Updation.Name = "lbl_Updation";
            this.lbl_Updation.Size = new System.Drawing.Size(168, 35);
            this.lbl_Updation.TabIndex = 11;
            this.lbl_Updation.Text = "Updation Form";
            // 
            // Tab_Delete
            // 
            this.Tab_Delete.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Tab_Delete.Controls.Add(this.panel2);
            this.Tab_Delete.Location = new System.Drawing.Point(4, 27);
            this.Tab_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.Tab_Delete.Name = "Tab_Delete";
            this.Tab_Delete.Padding = new System.Windows.Forms.Padding(4);
            this.Tab_Delete.Size = new System.Drawing.Size(727, 417);
            this.Tab_Delete.TabIndex = 3;
            this.Tab_Delete.Text = "Delete";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSkyBlue;
            this.panel2.Controls.Add(this.lbl_Col_Header_Del);
            this.panel2.Controls.Add(this.chkd_LstBx_Del);
            this.panel2.Controls.Add(this.lbl_Uid_Del);
            this.panel2.Controls.Add(this.btn_MarkAll_Del);
            this.panel2.Controls.Add(this.btn_Del);
            this.panel2.Controls.Add(this.btn_ViewAll_Del);
            this.panel2.Controls.Add(this.lbl_Del);
            this.panel2.Controls.Add(this.btn_Search_Del);
            this.panel2.Controls.Add(this.Txtbox_Search_Deletion);
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(728, 416);
            this.panel2.TabIndex = 17;
            // 
            // lbl_Col_Header_Del
            // 
            this.lbl_Col_Header_Del.AutoSize = true;
            this.lbl_Col_Header_Del.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Col_Header_Del.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_Col_Header_Del.Location = new System.Drawing.Point(97, 123);
            this.lbl_Col_Header_Del.Name = "lbl_Col_Header_Del";
            this.lbl_Col_Header_Del.Size = new System.Drawing.Size(458, 24);
            this.lbl_Col_Header_Del.TabIndex = 20;
            this.lbl_Col_Header_Del.Text = "UserID -> UserName -> FName -> LName -> Address";
            this.lbl_Col_Header_Del.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // chkd_LstBx_Del
            // 
            this.chkd_LstBx_Del.CheckOnClick = true;
            this.chkd_LstBx_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkd_LstBx_Del.FormattingEnabled = true;
            this.chkd_LstBx_Del.HorizontalScrollbar = true;
            this.chkd_LstBx_Del.Location = new System.Drawing.Point(61, 150);
            this.chkd_LstBx_Del.Name = "chkd_LstBx_Del";
            this.chkd_LstBx_Del.Size = new System.Drawing.Size(595, 208);
            this.chkd_LstBx_Del.TabIndex = 14;
            this.chkd_LstBx_Del.ThreeDCheckBoxes = true;
            this.chkd_LstBx_Del.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chkd_LstBx_Del_ItemCheck);
            // 
            // lbl_Uid_Del
            // 
            this.lbl_Uid_Del.AutoSize = true;
            this.lbl_Uid_Del.Location = new System.Drawing.Point(58, 73);
            this.lbl_Uid_Del.Name = "lbl_Uid_Del";
            this.lbl_Uid_Del.Size = new System.Drawing.Size(69, 17);
            this.lbl_Uid_Del.TabIndex = 17;
            this.lbl_Uid_Del.Text = "Enter UID";
            // 
            // btn_MarkAll_Del
            // 
            this.btn_MarkAll_Del.BackColor = System.Drawing.Color.Aquamarine;
            this.btn_MarkAll_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_MarkAll_Del.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_MarkAll_Del.Location = new System.Drawing.Point(196, 365);
            this.btn_MarkAll_Del.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_MarkAll_Del.Name = "btn_MarkAll_Del";
            this.btn_MarkAll_Del.Size = new System.Drawing.Size(101, 39);
            this.btn_MarkAll_Del.TabIndex = 15;
            this.btn_MarkAll_Del.Text = "Mark All";
            this.btn_MarkAll_Del.UseVisualStyleBackColor = false;
            this.btn_MarkAll_Del.Click += new System.EventHandler(this.btn_MarkAll_Del_Click);
            // 
            // btn_Del
            // 
            this.btn_Del.BackColor = System.Drawing.Color.Tomato;
            this.btn_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Del.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Del.Location = new System.Drawing.Point(430, 365);
            this.btn_Del.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(101, 39);
            this.btn_Del.TabIndex = 16;
            this.btn_Del.Text = "Delete";
            this.btn_Del.UseVisualStyleBackColor = false;
            this.btn_Del.Click += new System.EventHandler(this.btn_Del_Click);
            // 
            // btn_ViewAll_Del
            // 
            this.btn_ViewAll_Del.BackColor = System.Drawing.Color.Aqua;
            this.btn_ViewAll_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ViewAll_Del.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_ViewAll_Del.Location = new System.Drawing.Point(555, 62);
            this.btn_ViewAll_Del.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_ViewAll_Del.Name = "btn_ViewAll_Del";
            this.btn_ViewAll_Del.Size = new System.Drawing.Size(101, 39);
            this.btn_ViewAll_Del.TabIndex = 12;
            this.btn_ViewAll_Del.Text = "View All";
            this.btn_ViewAll_Del.UseVisualStyleBackColor = false;
            this.btn_ViewAll_Del.Click += new System.EventHandler(this.btn_ViewAll_Del_Click);
            // 
            // lbl_Del
            // 
            this.lbl_Del.AutoSize = true;
            this.lbl_Del.Font = new System.Drawing.Font("Segoe Print", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Del.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_Del.Location = new System.Drawing.Point(287, 17);
            this.lbl_Del.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Del.Name = "lbl_Del";
            this.lbl_Del.Size = new System.Drawing.Size(157, 35);
            this.lbl_Del.TabIndex = 11;
            this.lbl_Del.Text = "Deletion Form";
            // 
            // btn_Search_Del
            // 
            this.btn_Search_Del.BackColor = System.Drawing.Color.Aqua;
            this.btn_Search_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Search_Del.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Search_Del.Location = new System.Drawing.Point(430, 62);
            this.btn_Search_Del.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Search_Del.Name = "btn_Search_Del";
            this.btn_Search_Del.Size = new System.Drawing.Size(101, 39);
            this.btn_Search_Del.TabIndex = 10;
            this.btn_Search_Del.Text = "Search";
            this.btn_Search_Del.UseVisualStyleBackColor = false;
            this.btn_Search_Del.Click += new System.EventHandler(this.btn_Search_Del_Click);
            // 
            // Txtbox_Search_Deletion
            // 
            this.Txtbox_Search_Deletion.Location = new System.Drawing.Point(133, 70);
            this.Txtbox_Search_Deletion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txtbox_Search_Deletion.Name = "Txtbox_Search_Deletion";
            this.Txtbox_Search_Deletion.Size = new System.Drawing.Size(282, 22);
            this.Txtbox_Search_Deletion.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(732, 28);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "F";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(157, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.viewHelpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.viewHelpToolStripMenuItem.Text = "View Help";
            this.viewHelpToolStripMenuItem.Click += new System.EventHandler(this.viewHelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // FormCrud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(732, 476);
            this.Controls.Add(this.tabCtrl_crud);
            this.Controls.Add(this.panel_crud_tabs);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormCrud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration Form";
            this.tabCtrl_crud.ResumeLayout(false);
            this.tab_Reg.ResumeLayout(false);
            this.panel_Reg_Form.ResumeLayout(false);
            this.panel_Reg_Form.PerformLayout();
            this.tab_Retrieve.ResumeLayout(false);
            this.Panel_Retrival.ResumeLayout(false);
            this.Panel_Retrival.PerformLayout();
            this.tab_Update.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnl_Updation_Form.ResumeLayout(false);
            this.pnl_Updation_Form.PerformLayout();
            this.Tab_Delete.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_crud_tabs;
        private System.Windows.Forms.TabControl tabCtrl_crud;
        private System.Windows.Forms.TabPage tab_Reg;
        private System.Windows.Forms.Panel panel_Reg_Form;
        private System.Windows.Forms.Label lbl_Reg_Title;
        private System.Windows.Forms.RichTextBox rich_txt_box_address;
        private System.Windows.Forms.Button BtnRegister;
        private System.Windows.Forms.MaskedTextBox msk_txt_box_pass;
        private System.Windows.Forms.TextBox txt_box_lname;
        private System.Windows.Forms.TextBox txt_box_fname;
        private System.Windows.Forms.TextBox txt_box_usname;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_lname;
        private System.Windows.Forms.Label lbl_fname;
        private System.Windows.Forms.Label lbl_pass;
        private System.Windows.Forms.Label lbl_uname;
        private System.Windows.Forms.TabPage tab_Retrieve;
        private System.Windows.Forms.TabPage tab_Update;
        private System.Windows.Forms.TabPage Tab_Delete;
        private System.Windows.Forms.Panel Panel_Retrival;
        private System.Windows.Forms.Label Lbl_Title_Retrieve;
        private System.Windows.Forms.Button Btn_Search_Retrieval;
        private System.Windows.Forms.TextBox Txtbox_Search_Retrieval;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Updation;
        private System.Windows.Forms.Button Btn_All_Retrieval;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_MarkAll_Del;
        private System.Windows.Forms.Button btn_Del;
        private System.Windows.Forms.Button btn_ViewAll_Del;
        private System.Windows.Forms.Label lbl_Del;
        private System.Windows.Forms.Button btn_Search_Del;
        private System.Windows.Forms.TextBox Txtbox_Search_Deletion;
        private System.Windows.Forms.Label lbl_Uid_Del;
        private System.Windows.Forms.Panel pnl_Updation_Form;
        private System.Windows.Forms.RichTextBox rich_Txtbox_Address_Updation;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.MaskedTextBox msk_txtbox_Pass_Updation;
        private System.Windows.Forms.TextBox txtbox_Lname_Updation;
        private System.Windows.Forms.TextBox txtbox_Fname_Updation;
        private System.Windows.Forms.TextBox txtbox_Uname_Updation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_Search_Updation;
        private System.Windows.Forms.TextBox Txtbox_Search_Updation;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Reset_Reg;
        private System.Windows.Forms.ListBox lstbox_Retrieval;
        private System.Windows.Forms.CheckedListBox chkd_LstBx_Del;
        private System.Windows.Forms.Button btn_Reset_Updation;
        private System.Windows.Forms.Label lbl_Col_Header_Del;
    }
}

