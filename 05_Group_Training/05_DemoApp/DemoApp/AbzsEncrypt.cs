﻿using System;
using System.Text;

namespace PassEncrypt
{
    public static class AbzsCrypt
    {
        static string salt = "@Abzs123";

        public static void SetSalt(String usrSalt)
        {
            salt = usrSalt;
        }

        // Encryption Algorithm
        public static string Encrypt(StringBuilder passText)
        {
            for (int i = 0, j = 0; i < passText.Length; i++, j++)
            {
                if (j == salt.Length) j = 0;
                passText[i] = (char)((int)passText[i] + (int)salt[j]);
            }
            return passText.ToString();
        }

        // Decryption Algorithm
        public static string Decrypt(StringBuilder passHash)
        {
            for (int i = 0, j = 0; i < passHash.Length && j <= salt.Length; i++, j++)
            {
                if (j == salt.Length) j = 0;
                passHash[i] = (char)((int)passHash[i] - (int)salt[j]);
            }
            return passHash.ToString();
        }
    }
}
