﻿using PassEncrypt;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
    public partial class FormCrud : Form
    {
        string conStr = "Data Source=sql12-16.mt.cisinlive.com\\sql2012;Initial Catalog=dnntest;User ID=atul;Password=cis1234";
        bool flag = false;
        private HashSet<int> itemsChecked = new HashSet<int>();
        //AbzsCrypt.SetSalt("hey@hi1221");     //For setting salt by user (not working right now)
        //Member Variables for SqlRelated Operations
        SqlConnection conObj;
        SqlCommand cmd;
        SqlDataReader dataReader;

        public FormCrud()
        {
            InitializeComponent();
            conObj = new SqlConnection(conStr);
        }
        ~FormCrud()
        {
            conObj.Close();     //Even if App Exits Unexpectedly
        }

        //Main Menu items Event & Shortcut Keys Control Code Starts
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Designed & Developed at CIS.");
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Help Docs yet not prepared. Come back later.");
        }
        //Main Menu items Event & Shortcut Keys Control Code Ends

        //Registration/Creation operation (Insertion operation) Code Start
        private void BtnRegister_Click(object sender, EventArgs e)
        {
            string userName, pass, fname, lname, address;
            userName = txt_box_usname.Text;
            pass = AbzsCrypt.Encrypt(new StringBuilder(msk_txt_box_pass.Text));   // Apply encryption method from AbzsEncrypt Class Later
            fname = txt_box_fname.Text;
            lname = txt_box_lname.Text;
            address = rich_txt_box_address.Text;
            string querStr = "INSERT INTO Abzs_Register (UserName, Pwd, FName, LName, Address) VALUES('" + @userName + "','" + @pass + "','" + @fname + "','" + @lname + "','" + @address + "')";

            if (userName != "" && pass != "" && fname != "" && lname != "")
            {
                conObj.Open();  //Connection to DB opens
                if (conObj.State == System.Data.ConnectionState.Open)
                {
                    cmd = new SqlCommand(querStr, conObj);
                    cmd.ExecuteNonQuery();
                }
                MessageBox.Show("Your data has been inserted into database.");
                conObj.Close(); //Connection to DB closes
            }
            else
            {
                MessageBox.Show("Username, Passwords, First Name & Last Name are required fields.");
            }
        }

        private void btn_Reset_Reg_Click(object sender, EventArgs e)
        {
            txt_box_usname.Clear();
            msk_txt_box_pass.Clear();
            txt_box_fname.Clear();
            txt_box_lname.Clear();
            rich_txt_box_address.Clear();
        }
        //Registration/Creation operation (Insertion operation) Code Ends

        //Retrieval Form (Select operation) Code Starts
        private void Btn_Search_Retrieval_Click(object sender, EventArgs e)
        {
            //code for retreival of data from db.
            lstbox_Retrieval.Items.Clear();
            StringBuilder output = new StringBuilder();
            int uid = 0;
            try
            {
                uid = Convert.ToInt32(Txtbox_Search_Retrieval.Text);
            }
            catch
            {
                MessageBox.Show("Enter the UserID first, before searching.");
                return;
            }
            string querStr = "Select UserID, UserName, FName, LName, Address FROM Abzs_Register WHERE UserID = '" + @uid + "'";

            conObj.Open();  //Connection to DB opens
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                output.Append("UserID -> UserName -> FName -> LName -> Address");
                lstbox_Retrieval.Items.Add(output);
                output.Clear();
                for (int i = 0; i < 5; i++)
                {
                    output.Append(dataReader.GetValue(i) + "\t\t");
                }
                lstbox_Retrieval.Items.Add(output);
            }
            else
            {
                MessageBox.Show("The User ID you searched isn't available.");
            }
            conObj.Close(); //Connection to DB closes
        }

        private void Btn_All_Retrieval_Click(object sender, EventArgs e)
        {
            //code for retreival of all data from db.
            lstbox_Retrieval.Items.Clear();
            StringBuilder output = new StringBuilder();
            string querStr = "Select UserID, UserName, FName, LName, Address FROM Abzs_Register";

            conObj.Open();  //Connection to DB opens
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            //Condition checking that whether data exist in DB.
            if (dataReader.Read())
            {
                output.Append("UserID -> UserName -> FName -> LName -> Address");
                lstbox_Retrieval.Items.Add(output);
                output.Clear();
                //When multiple rows of data needs to be read
                do
                {
                    for (int i = 0; i < 5; i++)
                    {
                        output.Append(dataReader.GetValue(i) + "\t\t");
                    }
                    lstbox_Retrieval.Items.Add(output);
                    output.Clear();
                }
                while (dataReader.Read());
            }
            else
            {
                MessageBox.Show("There is no record available in our Database.");
            }
            conObj.Close(); //Connection to DB closes
        }
        //Retrieval Form (Select operation) Code Ends

        //Updation Form (Update operation) Code Starts
        private void btn_Search_Updation_Click(object sender, EventArgs e)
        {
            //code for retreival of data from db for updation.
            int uid = 0;
            try
            {
                uid = Convert.ToInt32(Txtbox_Search_Updation.Text);
            }
            catch
            {
                MessageBox.Show("Enter the UserID first, before searching.");
                return;
            }
            string querStr = "Select UserName, Pwd, FName, LName, Address FROM Abzs_Register WHERE UserID = '" + @uid + "'";

            conObj.Open();  //Connection to DB opens
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                txtbox_Uname_Updation.Text = dataReader.GetValue(0).ToString();
                msk_txtbox_Pass_Updation.Text = AbzsCrypt.Decrypt(new StringBuilder(dataReader.GetValue(1).ToString()));    // Apply Decryption method from AbzsEncrypt Class Later
                txtbox_Fname_Updation.Text = dataReader.GetValue(2).ToString();
                txtbox_Lname_Updation.Text = dataReader.GetValue(3).ToString();
                rich_Txtbox_Address_Updation.Text = dataReader.GetValue(4).ToString();
            }
            else
            {
                MessageBox.Show("The User ID you searched isn't available.");
            }

            conObj.Close(); //Connection to DB closes
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            int uid = 0;
            try
            {
                uid = Convert.ToInt32(Txtbox_Search_Updation.Text);
            }
            catch
            {
                MessageBox.Show("Enter the UserID first, before searching.");
                return;
            }
            string userName, pass, fname, lname, address;
            userName = txtbox_Uname_Updation.Text;
            pass = AbzsCrypt.Encrypt(new StringBuilder(msk_txtbox_Pass_Updation.Text));       // Applied Decryption password from AbzsEncrypt Class, will implement Later
            fname = txtbox_Fname_Updation.Text;
            lname = txtbox_Lname_Updation.Text;
            address = rich_Txtbox_Address_Updation.Text;
            string querStr = "UPDATE Abzs_Register SET UserName = '" + @userName + "', Pwd = '" + @pass + "', FName = '" + @fname + "', LName = '" + @lname + "', Address = '" + @address + "' WHERE UserID = '" + @uid + "'";


            if (userName != "" && pass != "" && fname != "" && lname != "")
            {
                conObj.Open();  //Connection to DB opens
                if (conObj.State == System.Data.ConnectionState.Open)
                {
                    cmd = new SqlCommand(querStr, conObj);
                    cmd.ExecuteNonQuery();
                }
                MessageBox.Show("Your data has been updated into database.");
                conObj.Close(); //Connection to DB closes
            }
            else
            {
                MessageBox.Show("Username, Passwords, First Name & Last Name are required fields.");
            }
        }

        private void btn_Reset_Updation_Click(object sender, EventArgs e)
        {
            Txtbox_Search_Updation.Clear();
            txtbox_Uname_Updation.Clear();
            msk_txtbox_Pass_Updation.Clear();
            txtbox_Fname_Updation.Clear();
            txtbox_Lname_Updation.Clear();
            rich_Txtbox_Address_Updation.Clear();
        }
        //Updation Form (Update operation) Code Ends

        //Deletion Form (Delete operation) Code Ends
        private void btn_Search_Del_Click(object sender, EventArgs e)
        {
            //code for retreival of data from db for deletion.
            chkd_LstBx_Del.Items.Clear();
            StringBuilder output = new StringBuilder();
            int uid = 0;
            try
            {
                uid = Convert.ToInt32(Txtbox_Search_Deletion.Text);
            }
            catch
            {
                MessageBox.Show("Enter the UserID first, before searching.");
                return;
            }
            string querStr = "Select UserID, UserName, FName, LName, Address FROM Abzs_Register WHERE UserID = '" + @uid + "'";

            conObj.Open();  //Connection to DB opens
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    output.Append(dataReader.GetValue(i) + "\t\t");
                }
                chkd_LstBx_Del.Items.Add(output);
            }
            else
            {
                MessageBox.Show("The User ID you searched isn't available.");
            }
            conObj.Close(); //Connection to DB closes
        }

        private void btn_ViewAll_Del_Click(object sender, EventArgs e)
        {
            //code for retreival of all data from db for deletion.
            btn_MarkAll_Del.Text = "Mark All";
            flag = false;
            chkd_LstBx_Del.Items.Clear();
            StringBuilder output = new StringBuilder();
            string querStr = "Select UserID, UserName, FName, LName, Address FROM Abzs_Register";

            conObj.Open();  //Connection to DB opens
            if (conObj.State == System.Data.ConnectionState.Open)
            {
                cmd = new SqlCommand(querStr, conObj);
                dataReader = cmd.ExecuteReader();
            }

            //Condition checking that whether data exist in DB.
            if (dataReader.Read())
            {
                //When multiple rows of data needs to be read
                do
                {
                    for (int i = 0; i < 5; i++)
                    {
                        //output.Append(dataReader.GetString(i) + "\t\t");          //Don't know whats happenning, GetString method takes in int and return string, but its giving error that not able to typecast int32 to string
                        //MessageBox.Show(dataReader.GetString((int)i));
                        output.Append(dataReader.GetValue(i) + "\t\t");
                    }
                    chkd_LstBx_Del.Items.Add(output.ToString());
                    output.Clear();
                }
                while (dataReader.Read());
            }
            else
            {
                MessageBox.Show("There is no record available in our Database.");
            }
            conObj.Close(); //Connection to DB closes
        }

        private void btn_MarkAll_Del_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                for (int i = 0; i < chkd_LstBx_Del.Items.Count; i++)
                {
                    chkd_LstBx_Del.SetItemChecked(i, false);
                }
                itemsChecked.Clear();
                btn_MarkAll_Del.Text = "Mark All";
                flag = false;
            }
            else
            {
                for (int i = 0; i < chkd_LstBx_Del.Items.Count; i++)
                {
                    chkd_LstBx_Del.SetItemChecked(i, true);
                    itemsChecked.Add(int.Parse(chkd_LstBx_Del.CheckedItems[i].ToString().Split()[0]));
                }
                if (chkd_LstBx_Del.CheckedItems.Count > 0)
                {
                    btn_MarkAll_Del.Text = "UnMark All";
                    flag = true;
                }
            }
        }

        private void chkd_LstBx_Del_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (chkd_LstBx_Del.CheckedItems.Count <= 1)     //Since when the last click to uncheck, the Variables value would be 1 after that it would become 0.
            {
                btn_MarkAll_Del.Text = "Mark All";
                flag = false;
            }
            if (chkd_LstBx_Del.CheckedItems.Count == chkd_LstBx_Del.Items.Count - 1)
            {
                btn_MarkAll_Del.Text = "UnMark All";
                flag = true;
            }

            /*Handling Exception, which was occuring due to chkd_LstBx_Del.SetItemChecked method in MarkAll Button click, 
             * which was triggering ItemCheckEventArgs and in it there is this method chkd_LstBx_Del.SelectedItem,
             * which used to remain uninitialized, since, no object was actually selected and therefore it was raising an exception.*/
            try
            {
                if (itemsChecked.Count > 0 && itemsChecked.Contains(int.Parse(chkd_LstBx_Del.SelectedItem.ToString()[0].ToString()))) //bug
                {
                    itemsChecked.Remove(int.Parse(chkd_LstBx_Del.SelectedItem.ToString()[0].ToString()));
                    MessageBox.Show(int.Parse(chkd_LstBx_Del.CheckedItems.ToString()[0].ToString()).ToString());
                }
                else
                {
                    itemsChecked.Add(int.Parse(chkd_LstBx_Del.SelectedItem.ToString().Split()[0].ToString()));
                }
            }
            catch()
            { }
        }

        private void btn_Del_Click(object sender, EventArgs e)
        {
            string querStr;
            if (chkd_LstBx_Del.CheckedItems.Count > 0)
            {
                conObj.Open();  //Connection to DB opens
                foreach (int i in itemsChecked)
                {
                    querStr = "DELETE FROM Abzs_Register WHERE UserID='" + @i + "'";
                    if (conObj.State == System.Data.ConnectionState.Open)
                    {
                        cmd = new SqlCommand(querStr, conObj);
                        cmd.ExecuteNonQuery();
                    }
                }
                conObj.Close(); //Connection to DB closes
                chkd_LstBx_Del.Items.Clear();
                MessageBox.Show("All Selected DataItems has been deleted from database.");
            }
            else
            {
                MessageBox.Show("No Item in DataBase.");
            }
        }
        //Deletion Form (Delete operation) Code Ends
    }
}
