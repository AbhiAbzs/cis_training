// https://www.geeksforgeeks.org/c-sharp-data-types/
// Demonstrates Value datatype -> Integral DataType
using System;

namespace dataTypesDemo
{
    class DataTypesDemo
    {
        static void Main(string[] args)
        {
            char ch = 'a';
            Console.Clear();
            do
            {
                Console.WriteLine("DataTypes Demo.\n");
                Console.WriteLine("Press 1 for Integral DataTypes Demo.");
                Console.WriteLine("Press 2 for Floating Point & Decimal DataTypes Demo.");
                Console.WriteLine("Press 3 for Character DataTypes Demo.");
                Console.WriteLine("Press 4 to exit.");
                ch = Console.ReadKey(true).KeyChar;
                switch (ch)
                {
                    case '1':
                        {
                            Console.WriteLine("1 pressed.");
                            IntegralDataTypesDemo.intDemo();
                            break;
                        }
                    case '2':
                        {
                            Console.WriteLine("2 pressed");
                            break;
                        }
                    case '3':
                        {
                            Console.WriteLine("3 pressed");
                            break;
                        }
                    case '4':
                        {
                            Console.WriteLine("4 pressed\n Exiting...");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("default");
                            break;
                        }
                }
                Console.Clear();
            } while (ch != '4');

        }
    }
    class IntegralDataTypesDemo
    {
        public static void intDemo()
        {
            Console.Clear();
            // Directly using any variables without assigning is not allowed.
            byte varByte;   // If not assigned, Error lineNo: Use of unassigned local variable 'varByte'  // Unsigned byte (default)
            short varShort = 255;
            int varInt = 123;
            long varLong = 321;
            //Int16 x = 2231; //Same as int, but difference is that int is a keyword, which uses Int16, which is a structure in System namespace;
            Console.WriteLine("\nC# data types are of 3 main types.\n 1.) Value DataType\n 2.) Reference DataType\n 3.) Pointer DataType.\n");
            Console.WriteLine("1.) Value Data types are of 4 types:\n 01) Signed & Unsigned Integral Types\t 02) Floating Point Types\t 03) Decimal Types\t 04) Character Types");
            Console.WriteLine();
            Console.WriteLine("Following are 4 default Integral dataTypes:\n\t a) byte(int8),\n\t b) short(int16),\n\t c) int(int32),\n\t d) long(int64)");  // Bytes are by default unsigned, while other 3 are signed.
            //Console.WriteLine(varByte = 5); //Current statement is allowed, but neither CWL(varByte) nor CWL(varByte +=5) is allowed.
            varByte = 5;    // Can't be given -ve value.
            Console.WriteLine("The values are\t{0} {1} {2} {3}.\n", varByte, varShort, varInt, varLong);

            // Integer Value dataType: 3(signed) & byte unsigned.
            sbyte varSByte = -123;  //Signed byte
            ushort varUShort = 32655;
            uint varUInt = 12;
            ulong varULong = 123123123;
            Console.WriteLine("Following are 4 Integer dataTypes:\n\t a) sbyte(int8),\n\t b) ushort(int16),\n\t c) uint(int32),\n\t d) ulong(int64)");
            Console.WriteLine("The values are\t{0} {1} {2} {3}.", varSByte, varUShort, varUInt, varULong);
            Console.WriteLine("Press any key to go back to main menu.");
            Console.ReadKey(true);

        }
    }
}