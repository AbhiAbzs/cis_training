/*
 * 11. Write a program to print word equivalent of a number from 0 to 9, e.g. 3 should be displayed as ‘Three’.
 **/

let val = 0,
    numInp = document.querySelector("#num"),
    display = document.querySelector("#display span"),
    records = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

// Code to take input and print its corresponding word.
numInp.addEventListener("input", (event) => {
    val = numInp.value;
    displayWord();
});

// Code to display the word for the entered value.
function displayWord() {
    if (val == "") {
        display.innerHTML = "";
    } else if (isNaN(parseInt(val))) {
        display.innerText = "not a number, please re-enter.";
    } else {
        display.innerHTML = records[val];
    }
}