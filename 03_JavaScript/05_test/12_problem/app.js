/*
 * 12. Write a program for counting occurrence of each character in a string.
 * For exp:  ANMOANPA
 * A – 3, N-2 , M – 1 like this
 **/

// let countCharBtn = document.querySelector("#countCharBtn");
let inpText = document.querySelector("#inpText"),
    display = document.querySelector("#display");

// Count characters and display on entering characters
inpText.addEventListener("input", () => {
    let countNum = countChars(inpText.value);
    display.innerText=countNum;
});

// Logic for counting characters & displaying it.
function countChars(inpStr) {
    let record = {};
    for (let index = 0; index < inpStr.length; index++) {
        if (record[inpStr[index]] == undefined) {
            record[inpStr[index]] = 1;
        } else {
            record[inpStr[index]] += 1;
        }
    }
    return Object.entries(record).join(" ");
}