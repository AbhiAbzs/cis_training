/*
 * 10. Write a JavaScript function that accepts a string as a parameter and
 * converts the first letter of each word of the string in upper case.
 **/

let inpText = document.querySelector("#inpText"),
    dataConverterBtn = document.querySelector("#dataConverterBtn"),
    display = document.querySelector("#display");

// Code to take input and print the corresponding data with the required logic.
dataConverterBtn.addEventListener("click", (event) => {
    let data = inpText.value;
    // console.log(data.split("<br />"));
    data = capatalize(data);
    data = capatalizeLn(data);
    display.innerText = data;

});

// Function to capatalize each words 1st character.
function capatalize(data) {
    let dataArr = data.split(" " || "<br />");
    for (let i = 0; i < dataArr.length; i++) {
        dataArr[i] = dataArr[i].charAt(0).toUpperCase() + dataArr[i].slice(1);
    }
    return dataArr.join(" ");
}

// Handling problem with enter pressed in text area.
function capatalizeLn(data){
    let dataArr = data.split("\n");
    for (let i = 0; i < dataArr.length; i++) {
        dataArr[i] = dataArr[i].charAt(0).toUpperCase() + dataArr[i].slice(1);
    }
    return dataArr.join("\n");
}