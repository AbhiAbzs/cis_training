﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Task3032019
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("forget password", "forgetpassword", "ok"); 
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("welcome","Login","ok");
        }
    }
}